# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_callhubuser_avatar'),
    ]

    operations = [
        migrations.CreateModel(
            name='OverallRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.DecimalField(default=0, null=True, max_digits=6, decimal_places=1)),
                ('number_of_ratings', models.IntegerField(default=0)),
                ('expert', models.OneToOneField(related_name='expert_overall_rating', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Overall Rating',
                'verbose_name_plural': 'Overall Ratings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField()),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now)),
                ('customer_user', models.ForeignKey(related_name='client_rating', to=settings.AUTH_USER_MODEL)),
                ('expert_user', models.ForeignKey(related_name='expert_rating', to=settings.AUTH_USER_MODEL)),
                ('overall_rating', models.ForeignKey(related_name='overall_rating', to='core.OverallRating', null=True)),
            ],
            options={
                'verbose_name': 'Rating',
                'verbose_name_plural': 'Ratings',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='callhubuser',
            name='avatar',
            field=models.CharField(default=b'', max_length=b'255', null=True, verbose_name=b'Social Media Avatar', blank=True),
            preserve_default=True,
        ),
    ]

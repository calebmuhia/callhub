# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Slot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.DateTimeField(verbose_name='start')),
                ('end', models.DateTimeField(help_text='The end time must be later than the start time.', verbose_name='end')),
                ('title', models.CharField(default=b'', max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=b'Updated On')),
                ('scheduled', models.BooleanField(default=False)),
                ('allDay', models.BooleanField(default=False)),
                ('customer', models.ForeignKey(related_name='customer', verbose_name=b'Customer', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('expert', models.ForeignKey(related_name='expert', verbose_name=b'Expert', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Slot',
                'verbose_name_plural': 'Slots',
            },
            bases=(models.Model,),
        ),
    ]

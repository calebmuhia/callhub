# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_callhubuser_timezone'),
    ]

    operations = [
        migrations.AddField(
            model_name='callhubuser',
            name='avatar',
            field=models.CharField(default=b'', max_length=b'255', null=True, blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_slot'),
    ]

    operations = [
        migrations.AlterField(
            model_name='callhubuser',
            name='user_type',
            field=models.CharField(default=b'Client', max_length=b'255', choices=[(b'Expert', b'Expert'), (b'Client', b'Client')]),
            preserve_default=True,
        ),
    ]

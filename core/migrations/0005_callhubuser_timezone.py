# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import timezone_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_callhubuser_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='callhubuser',
            name='timezone',
            field=timezone_field.fields.TimeZoneField(blank=True, null=True),
            preserve_default=True,
        ),
    ]

from core.api import *
from tastypie.serializers import Serializer
import ast
import pytz
from datetime import datetime, timedelta
from django.utils import timezone
import json
# import iso8601

class RatingResource(ModelResource):

    class Meta:

        queryset = Rating.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get',]

        resource_name = 'rating'
        authentication = MyBasicAuthentication()
        authorization = Authorization()

class OverallRatingResource(ModelResource):
    #ratings = fields.ToManyField('core.api.api.RatingResource', 'overall_rating', null=True, full=True)
    class Meta:

        queryset = OverallRating.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get',]

        resource_name = 'ratings'
        authentication = MyBasicAuthentication()
        authorization = Authorization()
        

    # def get_object_list(self, request):
        
    #     return super(ExpertSearchResource, self).get_object_list(request).filter(user_type='Expert')   


def today(user_timezone):
    """
    Returns a tuple of two datetime instances: the beginning of today, and the
    end of today.
    """
    now = timezone.make_aware(datetime.now(), timezone.get_current_timezone())
    now = user_timezone.normalize(now.astimezone(user_timezone))
    today_start = datetime.min.replace(year=now.year, month=now.month,
                                 day=now.day)
    today_end = (today_start  + timedelta(days=1)) - timedelta.resolution
    
    return (now, today_end)


class UserResource(ModelResource):

    image = Base64FileField('image')


    class Meta:

        queryset = User.objects.all()
        list_allowed_methods = ['get','put', 'head', 'delete']
        detail_allowed_methods = ['get','put', 'head', 'delete']

        resource_name = 'user'
        authentication = MyBasicAuthentication()
        authorization = Authorization() 
        filtering={
        'slug':ALL
        }

        excludes=['password', 'is_staff','is_superuser']
        always_return_data = True

    def hydrate_image(self, bundle):

        try:
            if bundle.data['image'] and isinstance(bundle.data['image'], dict):
                
                bundle.obj.image = SimpleUploadedFile(bundle.data['image']["name"],
                                          base64.b64decode(bundle.data['image']["file"]),
                                          bundle.data['image'].get("content-type",
                                                    "application/octet-stream"))
                

                return bundle
            
            else:
                bundle.data['image'] = ''
                return bundle
        except:
            bundle.data['image'] = ''
            return bundle    

    def dehydrate(self, bundle):
        
        bundle.data['country_name'] = bundle.obj.country.name
        bundle.data['timezone_repr'] = repr(bundle.obj.timezone).replace('<', '').replace('>', '')

        return bundle 
        

        return bundle     

    def get_object_list(self, request):
        return super(UserResource, self).get_object_list(request).filter(username=request.user.username)

class ExpertResource(ModelResource):

    rating = fields.ToOneField('core.api.api.OverallRatingResource', 'expert_overall_rating', null=True, full=True)

    class Meta:

        queryset = User.objects.filter(user_type="Expert")
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get',]

        resource_name = 'experts'
        authentication = MyBasicAuthentication()
        authorization = Authorization()
        excludes = ['password', 'is_staff', 'is_superuser','account_number_sid', "account_phone_number","address", "date_joined", "user_type", "username"  ]
        filtering = {
        'slug':ALL
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<id>[\d]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),

        ]    

    def dehydrate(self, bundle):
        
        try:
            
            if not bundle.data['rating']:
                
                bundle.data['rating'] = {
                        
                        "number_of_ratings": 0,
                        "rating": "0",
                        }
        except Exception as e:
            print e
            pass        
        return bundle

class ExpertSlotResource(ModelResource):
    expert = fields.ForeignKey(UserResource, 'expert')
    customer = fields.ForeignKey(UserResource, 'customer', null=True, blank=True)
    # className = fields.CharField(attribute="className", readonly=True, null=True, blank=True)

    '''
    this is the api resource for the event
    '''

    # link to the occurence object

    class Meta:
        queryset = Slot.objects.all()
        resource_name = 'expertslots'
        authentication = MyBasicAuthentication()
        authorization = Authorization()
        filtering = {
                 'start':ALL,
                 'end':ALL,
                 'expert':ALL_WITH_RELATIONS
        }
        limit = 0


        

    def hydrate(self, bundle):
    
        bundle.obj.expert = bundle.request.user

        return bundle

    def dehydrate(self, bundle):
        # bundle.data["className"] = ast.literal_eval(bundle.data['className']) 

        user = bundle.obj.expert
        try:
            user_timezone = user.timezone



            if user_timezone:
                
                start = user_timezone.normalize(bundle.obj.start.astimezone(user_timezone))
                end = user_timezone.normalize(bundle.obj.end.astimezone(user_timezone))

                (today_start,today_end) = today(user_timezone)
                today_start = today_start
                today_end = timezone.make_aware(today_end, user_timezone)

                
                classname = ['event']


                if end > today_start and end < today_end:
                    
                    
                    classname.append('bg-color-orange')

                    
                elif (end > today_end):
                    classname = ["event", "bg-color-blue"]

                else:
                    classname = ['event', 'bg-color-red']


                bundle.data['className'] = classname



                bundle.data["start"] = start.strftime('%Y-%m-%dT%H:%M:%S%z')
                bundle.data["end"] = end.strftime('%Y-%m-%dT%H:%M:%S%z')

        except Exception, e:
            print "error", e

        return bundle    

    def get_object_list(self, request):

        return super(ExpertSlotResource, self).get_object_list(request).filter(expert=request.user)    



class dict2obj(object):
    """
    Convert dictionary to object
    @source http://stackoverflow.com/a/1305561/383912
    """
    def __init__(self, d):
        self.__dict__['d'] = d
 
    def __getattr__(self, key):
        value = self.__dict__['d'][key]
        if type(value) == type({}):
            return dict2obj(value)
 
        return value



class CountryResource(Resource):

    name = fields.CharField(attribute = 'name')
    code = fields.CharField(attribute = 'code')

    class Meta:
        resource_name = 'countries'
        limit=0




    def obj_get_list(self, request=None, **kwargs):

        country_list = []
        for code, name in list(countries):
            country_list.append(dict2obj({"name":unicode(name), "code":code}))

        return country_list


class TimeZoneResource(Resource):

    rep = fields.CharField(attribute = 'rep')
    tz = fields.CharField(attribute = 'tz')
    limit = 0

    class Meta:
        resource_name = 'timezones'

    def obj_get_list(self, request=None, **kwargs):
        NOW = DT.datetime.now()
        ZERO = DT.timedelta(0)
        tz_list = []
        for tname in pytz.common_timezones:
            tzone = pytz.timezone(tname)
            std_date = None
            try:
                for utcdate, info in zip(
                        tzone._utc_transition_times, tzone._transition_info):
                    utcoffset, dstoffset, tzname = info
                    if dstoffset == ZERO:
                        std_date = utcdate
                    if utcdate > NOW:
                        break
            except AttributeError:
                std_date = NOW
            std_date = tzone.localize(std_date)    
            tz_list.append(dict2obj({'tz':tname,'rep':'{0} UTC{1}'.format(tname, std_date.strftime('%z'))}))
            
        return tz_list



from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.exceptions import BadRequest
from tastypie import fields


from django.conf.urls import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
#from haystack.query import SearchQuerySet
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash

from tastypie.constants import ALL, ALL_WITH_RELATIONS
from core.api.authenticator import MyBasicAuthentication

from core.models import CallHubUser as User, Slot, Rating, OverallRating
from core.api.b64field import Base64FileField
import pytz, base64, os, mimetypes, datetime as DT

import base64
import os
import mimetypes
import pytz
import datetime as DT

from django.core.files.uploadedfile import SimpleUploadedFile
from django_countries import countries





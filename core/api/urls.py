from core.api.api import ExpertSlotResource, UserResource, CountryResource, TimeZoneResource, ExpertResource
from tastypie.api import Api
from django.conf.urls import patterns, include, url
v1_api = Api(api_name='v1')
v1_api.register(ExpertSlotResource())
v1_api.register(UserResource())
v1_api.register(CountryResource())
v1_api.register(TimeZoneResource())
v1_api.register(ExpertResource())





urlpatterns = patterns('core.views',

    url(r'^api/', include(v1_api.urls)),

)
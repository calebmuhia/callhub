from django.conf.urls import patterns, include, url
from core.views.expert import Dashboard  

urlpatterns = patterns('',
    url(r'^expert/', Dashboard.as_view(), name="expert_dashboard" )

)
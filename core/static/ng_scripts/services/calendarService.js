"use strict"

angular.module('callHubApp.services.calendar', [])

.factory("CalendarService", ['$q', '$http', '$rootScope', 'Expertslots', '$compile', 'uiCalendarConfig',
    function($q, $http, $rootScope, Expertslots, $compile, uiCalendarConfig) {

        var exports = {}

        // fetching events for the current user, we dont pass the user id 
        // coz the api will only fetch the data for the loged in user
        exports.eventsF = function(start, end, callback) {
            console.log("function called")
            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();

            var events = Expertslots.get(function(response) {
                console.log(response.objects)
                callback(response.objects)
            })

        }; // scope.eventsf


        //emit an event when a slot is clicked for redirects and creating new slots
        var OnSlotClick = function(start, end) {

            $rootScope.$emit("calendarSlotClicked", start, end)

        }; //OnSlotClick()

        var dayClick = function(date, jsEvent, view) {
            console.log(jsEvent)
            $rootScope.$emit("calendarDayClicked", date, jsEvent, view)


        }


        /* Change View */
        var changeView = function(view, calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        }; // changeView()



        /* Change View */
        var renderCalender = function(calendar) {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        }; // renderCalender()


        

        /* config object */
        exports.uiConfig = {
            calendar: {
                height: 700,
                editable: true,
                header: {
                    left: 'title',
                    center: '',
                    right: 'today prev,next'
                },
            }
        }; // uiConfig


        return exports


    }
])
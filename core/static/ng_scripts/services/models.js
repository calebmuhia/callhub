var models_module = angular.module('callHubApp.services.models', [])

var resources = [
    'User',
    'Expertslots',
    'Countries',
    'Timezones',
    'Experts'
    ]
    angular.forEach(resources, function(s, index){
        
        try{
            models_module.factory(s, ['$resource','$rootScope', function ($resource,$rootScope) {


            return $resource('/api/v1/' + camelToDash(s) + '/:id/',
                { id: '@id' },
                {
                    update: { method: 'PUT' },
                    get: {isArray: false, method: 'GET'},
                    query: {method:'GET', params:{}},
                    delete: { method: 'DELETE' },
                    post: { method: 'POST' }
                });
        }]);
        }
        catch(e){
            console.log(e)
        }
        
    });
'use strict'


angular.module("experts.urls", [])

.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/dashboard/home');

        $stateProvider
        // home
        .state('dashboard', {
            url: '/dashboard',
            views: {
                '': {
                    templateUrl: '/static/partials/experts/base.html'
                },
                'nav@dashboard': {
                    templateUrl: '/static/partials/experts/nav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state
                    },
                },
                'subnav@dashboard': {
                    templateUrl: '/static/partials/experts/subnav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state
                    },
                }

            }//views

        }) //dashboard


        // dashboard.home
        .state('dashboard.home', {
            url: '/home',
            views: {
                'content@dashboard': {
                    templateUrl: '/static/partials/experts/dashboard/home.html',
                    controller: 'DashboardCtrl'
                },

            }//views
        }) //dashboard.home


        // dashboard.manage_slots
        .state('dashboard.manage_slots', {
            url: '/manage-slots',
            views: {
                'content@dashboard': {
                    templateUrl: '/static/partials/experts/dashboard/manage_slots.html',
                    controller: 'ManageSlotsCtrl'
                },

            }//views
        }) //dasboard.manage_slots


        // manage_slot
        .state('dashboard.manage_slot', {
            url: '/manage-slot/:slotId/',
            views: {
                'content@dashboard': {
                    templateUrl: '/static/partials/experts/dashboard/manage_slot.html',
                    controller: 'ManageSlotCtrl'
                },

            }
        }) //manage_slots.slot


        // usersettings
        .state('usersettings', {
            url: '/usersettings',
            views: {
                '': {
                    templateUrl: '/static/partials/experts/base.html'
                },
                'nav@usersettings': {
                    templateUrl: '/static/partials/experts/nav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state;
                       
                    },
                },
                'subnav@usersettings': {
                    templateUrl: '/static/partials/experts/subnav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state;
                       
                    },
                }

            }//views

        }) //usersettings


        // usersettings.account
        .state('usersettings.account', {
            url: '/account',
            views: {
                'content@usersettings': {
                    templateUrl: '/static/partials/experts/account/account_base.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state;
                       
                    },
                },

            }//views
        }) //usersettings.account

        // usersettings.account.edit_profile
        .state('usersettings.account.edit_profile', {
            url: '/edit_profile',
            views: {
                'account_content@usersettings.account': {
                    templateUrl: '/static/partials/experts/account/edit_profile.html',
                    controller: 'EditUserProfileCtrl'
                },

            }//views
        }) //usersettings.account.edit_profile

        // usersettings.account.edit_image
        .state('usersettings.account.edit_image', {
            url: '/edit_image',
            views: {
                'account_content@usersettings.account': {
                    templateUrl: '/static/partials/experts/account/edit_image.html',
                    controller: 'EditUserProfileCtrl'
                },

            }//views
        }) //usersettings.account.edit_profile



        




    }
])
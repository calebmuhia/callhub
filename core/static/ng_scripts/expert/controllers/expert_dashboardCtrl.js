angular.module('callHubApp')

.controller('DashboardCtrl', ['$scope', '$compile', 'uiCalendarConfig', 'AuthUser', 'User', 'CalendarService', '$rootScope','$state',
    function($scope, $compile, uiCalendarConfig, AuthUser, User, CalendarService, $rootScope, $state) {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        // catch a clicked slot
        $scope.onEventClick = function(event, jsEvent, view) {
            $state.go('dashboard.manage_slot', {slotId:event.id})
        }

        /* Render Tooltip */
        var eventRender = function(event, element, view) {
            element.attr({'tooltip': "Click to edit "+event.title,
                         'tooltip-append-to-body': true});
            element[0].querySelector('.fc-event-skin').style.cursor = 'pointer';
            $compile(element)($scope);
        }; //eventRender()


        //initialize the calendar with config
        $scope.uiConfig = CalendarService.uiConfig
        var extra_config = {

                defaultView: "month",
                editable: false,
                allDaySlot: false,
                selectable: true,
                selectHelper: true,
                // select: $scope.onSlotSelect,
                eventClick: $scope.onEventClick,
                eventRender:eventRender

            } // extra_config

        angular.forEach(extra_config, function(val, key) {

            $scope.uiConfig.calendar[key] = val
        }); //forEach()

        // console.log($scope.uiConfig)

        //get the events source is the api
        $scope.eventSources = [CalendarService.eventsF];




    }
]);
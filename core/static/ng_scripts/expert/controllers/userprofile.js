'use strict'

angular.module('callHubApp')

.controller('EditUserProfileCtrl', ['$scope', 'User', 'Countries', 'Timezones', '$cookies', 'AuthUser', '$modal',
    function($scope, User, Countries, Timezones, $cookies, AuthUser, $modal) {

        $scope.editaccount = false;



        $scope.editaccountinfo = function() {
            $scope.editaccount = true
        } //editaccountinfo()



        Countries.get({
            limit: 0
        }).$promise
            .then(function(countries) {

                $scope.countries = countries.objects
            }) //then()

        Timezones.get({
            limit: 0
        }).$promise
            .then(function(timezones) {
                $scope.timezones = timezones.objects
            }) //then()




        $scope.getUserData = function(object) {

            User.get({
                id: AuthUser.id
            }).$promise
                .then(function(object) {

                    var data = object;
                    $scope.updateDomUser(data)




                })
        } //getUserData()  

        $scope.getUserData()

        $scope.updateDomUser = function(object) {

            //lets deep copy the original data
            $scope.originalUserObject = object


            if ($scope.originalUserObject.image) {
                $scope.updateDomImage(angular.copy($scope.originalUserObject))

                delete $scope.originalUserObject.image
            }
            $scope.copieduserObject = angular.copy($scope.originalUserObject);

        } //updateDomUser()

        $scope.selected_country = function(code) {
            console.log(code)

            angular.forEach($scope.countries, function(object) {

                if (object.code == code) {
                    console.log('found', object.code, object.name)
                    $scope.copieduserObject.country_name = object.name
                    $scope.originalUserObject.country_name = object.name
                }
            })


        } //selected_country()


        $scope.updateDomImage = function(object) {
            console.log(object)




            if (object.image.file) {
                $scope.add_image = false

                $scope.userprofilepic = 'data:image/png;base64,' + object.image.file;

            } else {
                $scope.add_image = (object.image == '/media/profile_pic/default.jpg')
                $scope.userprofilepic = object.image;
            }

            console.log(object)

        }

        $scope.cancelUserUpdate = function() {
            $scope.copieduserObject = angular.copy($scope.originalUserObject);
            $scope.editaccount = false;

        } //cancelUserUpdate


        $scope.updateUserInfo = function() {
            var data = $scope.copieduserObject
            console.log(data)

            User.update(data).$promise
                .then(function(res) {
                    console.log('success res', res)

                    $scope.updateDomUser(res)
                    $scope.selected_country(res.country)
                    $scope.editaccount = false;

                }, function(res) {
                    console.log('error res', res)
                })


        } //updateUserInfo()


        //will be called after crop is complete
        $scope.$on("cropme:done", function(e, blob, canvasEl) {
            var reader = new window.FileReader();
            var base64data = ''
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result.replace('data:image/png;base64,', '');
                var image = {
                        name: 'default1.png',
                        file: base64data,
                        'content-type': "image/png",
                    } //image

                try {
                    var data = {
                        id: $scope.originalUserObject.id,
                        resource_uri: $scope.originalUserObject.resource_uri,
                        image: image
                    }; //data



                    console.log(data)
                    $scope.savedUser = User.update(data, function(res) {
                            console.log('success res', res)
                            $scope.updateDomImage(res)


                            $scope.modalInstance.dismiss('cancel');

                        }, function(res) {
                            console.log('error res', res)
                        } //function()
                    ) //update()


                } //try
                catch (e) {
                    console.log(e)
                } //catch

            }




        }); //$on()

        $scope.open = function(size) {

            $scope.modalInstance = $modal.open({
                templateUrl: 'uploadpic.html',
                controller: 'UploadPicModalCtrl',
                size: size,

            });

            $scope.modalInstance.result.then(function() {}, function() {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };







    } //function()
])

.controller('UploadPicModalCtrl', ['$scope', '$modalInstance',
    function($scope, $modalInstance) {



        $scope.ok = function() {
            $modalInstance.close()

        }
        $scope.cancel = function() {
            $modalInstance.dismiss()
        }

    }
])
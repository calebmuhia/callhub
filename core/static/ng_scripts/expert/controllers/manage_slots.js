"use strict"
angular.module('callHubApp')

.controller('ManageSlotsCtrl', ['$scope', '$compile', 'uiCalendarConfig', 'AuthUser', 'User', 'CalendarService', '$rootScope', "$modal", 'Expertslots', '$state',
    function($scope, $compile, uiCalendarConfig, AuthUser, User, CalendarService, $rootScope, $modal, Expertslots, $state) {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $scope.open = function(size, appointmentObject) {
            //open a modal 

            var modalInstance = $modal.open({
                templateUrl: 'createEmptySlot.html',
                controller: 'CreateEmptySlotCtrl',
                size: size,
                resolve: {
                    SlotData: function() {

                        return appointmentObject;

                    }
                }
            });

            modalInstance.result.then(function(slotData) {
                slotData.expert = "/api/v1/user/" + AuthUser.id + '/'

                Expertslots.post(slotData).$promise
                    .then(function(res) {
                        console.log(res)
                        uiCalendarConfig.calendars['myCalendar'].fullCalendar('renderEvent', slotData, true);
                        // toaster.pop('success', "Success", "Successfully created Empty Slot!");

                    }); //post()


            }, function() {
                //console.log('Modal dismissed at: ' + new Date());
            }); //.then()

        }; // open()

        $scope.onEventClick = function(event, jsEvent, view) {
            $state.go('dashboard.manage_slot', {
                slotId: event.id
            })
        }

        /* Render Tooltip */
        var eventRender = function(event, element, view) {
            element.attr({
                'tooltip': "Click to edit " + event.title,
                'tooltip-append-to-body': true
            });
            element[0].querySelector('.fc-event-skin').style.cursor = 'pointer';
            $compile(element)($scope);
        }; //eventRender()


        $scope.onSlotSelect = function(start, end) {



            var today = new Date();
            today.setHours(0, 0, 0, 0);
            if ((today * 1) > (start * 1)) {
                // toaster.pop('error', "Error", "Cannot create empty slot on past day!");
                uiCalendarConfig.calendars['myCalendar'].fullCalendar('unSelect');

                return
            } //if
            if (start * 1 == end * 1) {
                // $scope.toasterPop('error', 'error', 'Please use the week view to create empty slots');
                toaster.pop('error', "Error", "Please use the week view to create empty slots!");
                uiCalendarConfig.calendars['myCalendar'].fullCalendar('unSelect');

                return
            } //if  

            var eventData = {
                title: 'Empty Slot',
                start: start,
                end: end,
                className: ["event", "bg-color-blue"],
                allDay: false

            }; //eventData

            $scope.open('sm', eventData)



        }; //onSlotSelect

        //initialize the calendar with config
        $scope.uiConfig = CalendarService.uiConfig
        var extra_config = {
                defaultView: "agendaWeek",
                slotDuration: "00:30:00",
                editable: true,
                allDaySlot: false,
                selectable: true,
                selectHelper: true,
                select: $scope.onSlotSelect,
                eventClick: $scope.onEventClick,
                eventRender: eventRender

            } // extra_config

        angular.forEach(extra_config, function(val, key) {

            $scope.uiConfig.calendar[key] = val
        }); //forEach()

        // console.log($scope.uiConfig)



        //get the events source is the api
        $scope.eventSources = [CalendarService.eventsF];
        // catch a clicked slot

    }
])



// creating a modal instance for creating and empty slot where it was clicked
.controller('CreateEmptySlotCtrl', ['$scope', 'SlotData', '$modalInstance',
    function($scope, SlotData, $modalInstance) {

        $scope.SlotData = SlotData



        $scope.ok = function() {
            $modalInstance.close(SlotData)

        }
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel')
        }

    }
])
'use strict'


angular.module("callHubClientApp")


.controller('ViewExpertCtrl', ['$scope','Experts', '$stateParams',
    function($scope, Experts, $stateParams){
        

    Experts.get({id:$stateParams.user_slug, limit:15}).$promise
    .then(function(response){

        $scope.expert = response

    })
    
}])
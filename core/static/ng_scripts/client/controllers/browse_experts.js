'use strict'


angular.module("callHubClientApp")


.controller('BrowseExpertsCtrl', ['$scope','Experts', 
    function($scope, Experts){

    Experts.get({limit:15}).$promise
    .then(function(response){

        $scope.experts = response.objects
        $scope.meta = response.meta
    })
    
}])
'use strict'

angular.module('callHubClientApp', [
    'ui.router',
    'ngResource',
    'ngRoute',
    'ngCookies',
    'ui.bootstrap',
    'ui.calendar',
    'ngAnimate',
    'ngSanitize',

    //we will include urls files here to keep the code base neat and organised
    'client.urls', // /static/ng_scripts/config/urls.js
    //calendar services
    'callHubApp.services.calendar',
    //resource apis models
    'callHubApp.services.models'
    

    

])


.config(['$resourceProvider', '$interpolateProvider', '$httpProvider',
    function($resourceProvider, $interpolateProvider, $httpProvider) {
        // Don't strip trailing slashes from calculated URLs
        $resourceProvider.defaults.stripTrailingSlashes = false;

        // Force angular to use square brackets for template tag
        // The alternative is using {% verbatim %}
        // $interpolateProvider.startSymbol('[[').endSymbol(']]');

        // CSRF Support
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'HTTP_X-CSRFToken';
      }

        
])
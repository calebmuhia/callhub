'use strict'


angular.module("client.urls", [])

.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/dashboard/home');

        $stateProvider
        // home
        .state('dashboard', {
            url: '/dashboard',
            views: {
                '': {
                    templateUrl: '/static/partials/clients/base.html'
                },
                'nav@dashboard': {
                    templateUrl: '/static/partials/clients/nav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state
                    },
                },
                'subnav@dashboard': {
                    templateUrl: '/static/partials/clients/subnav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state

                    },
                }

            }

        }) //dashboard


        // dashboard.home
        .state('dashboard.home', {
            url: '/home',
            views: {
                'content@dashboard': {
                    templateUrl: '/static/partials/clients/dashboard/home.html',
                    
                },

            }
        }) //dashboard.home


        // browse_expert
        .state('browse_expert', {
            url: '/browse_expert',

            views: {
                '': {
                    templateUrl: '/static/partials/clients/base.html',
                    controller: function($scope){
                        $scope.hide_subnav = true
                    }
                },
                'nav@browse_expert': {
                    templateUrl: '/static/partials/clients/nav.html',
                    controller: function($scope, $state) {
                        $scope.$state = $state
                    },
                },
                

            }
        }) //browse_expert


        // browse_expert.experts
        .state('browse_expert.experts', {
            url: '/experts',

            views: {
                'content@browse_expert': {
                    templateUrl: '/static/partials/clients/experts/experts_list.html',
                    controller: 'BrowseExpertsCtrl'
                },
                

            }
        }) //browse_expert.experts

        // browse_expert.experts
        .state('browse_expert.view_expert', {
            url: '/view_profile/:user_slug',

            views: {
                'content@browse_expert': {
                    templateUrl: '/static/partials/clients/experts/view_expert.html',
                    controller: 'ViewExpertCtrl'
                },
                

            }
        }) //browse_expert.experts


        

        




    }
])
from django.contrib import admin

from django.contrib.auth.admin import UserAdmin

from django.utils.translation import ugettext_lazy as _
from core.forms import CustomUserChangeForm, CustomUserCreationForm
from core.models import CallHubUser, Slot, Rating, OverallRating
# Register your models here.

class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    fieldsets = (
        (None, {'fields': ('username','email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Profile Fields'),{'fields':('call_rate', 'about', 'image','avatar','user_type','headline','industry','current_status')}),
        (_('Phone Numbers'),{'fields':('mobile_number','account_phone_number','account_number_sid')}),
        (_('Location'),{'fields':('address','country', 'timezone')}),

    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2')}
        ),
    )
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('username','email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


class RatingAdmin(admin.StackedInline):
    model = Rating
    extra = 0

class OverallRatingAdmin(admin.ModelAdmin):
    inlines = (RatingAdmin,)    



admin.site.register(OverallRating, OverallRatingAdmin)    
admin.site.register(CallHubUser, CustomUserAdmin)
admin.site.register(Slot)


import json
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
import urllib2, os
def get_user_avatar(backend, details, response, uid, user, *args, **kwargs):
    social = kwargs.get('social') or \
             backend.strategy.storage.user.get_social_auth(backend.name, uid)
    url = None
    if backend.name == 'facebook':
        url = "http://graph.facebook.com/%s/picture?type=large" % response['id']

    if url:
        details.update({'avatar': url})

               

# def get_user_location(strategy, details, response, uid, user, *args, **kwargs):
#     social = kwargs.get('social') or strategy.storage.user.get_social_auth(
#         strategy.backend.name,
#         uid
#     )

#     location = None

#     if strategy.backend.name == 'facebook':



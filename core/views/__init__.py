__author__ = 'caleb'
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_GET
from django.http import HttpResponse, HttpResponseRedirect
from core.models import *
from django.contrib.auth import login, authenticate

from decimal import Decimal


from datetime import datetime

import json
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator



class LoginRedirectView(RedirectView):

    permanent = False
    query_string = True

    def get_redirect_url(self ):
        print "logged in"



        user = self.request.user
        if user and user.is_authenticated():
            if user.user_type == 'Expert':
                return reverse('expert_dashboard')
            else:
                return reverse('client_dashboard')
        else:
            print "not a user"
            return reverse('auth_login')


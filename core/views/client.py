__author__ = "caleb"
from core.views import *


def not_client(function=None):
  actual_decorator = user_passes_test(
    lambda u: u.user_type == 'Client', login_url = '/accounts/login'
  )

  return actual_decorator(function)

class ClientLoginRequiredMixin(object):
    """
    this class checkes for authenticated expert users
    will redirect any user not an expert
    """
    


    @method_decorator(login_required(login_url='/accounts/login'))
    @method_decorator(not_client)
    def dispatch(self, request, *args, **kwargs):

        return super(ClientLoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class Dashboard(ClientLoginRequiredMixin,TemplateView):
    """
    dashboard view for the expert

    """
    template_name = 'core/client/index.html'

    def get(self, request, *args, **kwargs):
        ctx = super(Dashboard, self).get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
        return render_to_response(self.template_name, ctx)

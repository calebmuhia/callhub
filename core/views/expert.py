__author__ = "caleb"
from core.views import *


def not_expert(function=None):
  actual_decorator = user_passes_test(
    lambda u: u.user_type == 'Expert', login_url = '/accounts/login'
  )

  return actual_decorator(function)

class ExpertLoginRequiredMixin(object):
    """
    this class checkes for authenticated expert users
    will redirect any user not an expert
    """
    


    @method_decorator(login_required(login_url='/accounts/login'))
    @method_decorator(not_expert)
    def dispatch(self, request, *args, **kwargs):

        return super(ExpertLoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class Dashboard(ExpertLoginRequiredMixin,TemplateView):
    """
    dashboard view for the expert

    """
    template_name = 'core/expert/index.html'

    def get(self, request, *args, **kwargs):
        ctx = super(Dashboard, self).get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
        return render_to_response(self.template_name, ctx)


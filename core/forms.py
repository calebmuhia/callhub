__author__ = 'caleb'
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from core.models import CallHubUser
from django import forms

class CustomUserCreationForm(UserCreationForm):
    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            CallHubUser._default_manager.get(username=username)
        except CallHubUser.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    class Meta(UserCreationForm.Meta):
        model = CallHubUser

class CustomUserChangeForm(UserChangeForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserChangeForm, self).__init__(*args, **kargs)


    class Meta:
        model = CallHubUser
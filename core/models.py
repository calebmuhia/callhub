from django.db import models
from django.utils import timezone

# Create your models here.
from django.contrib.auth.models import AbstractUser, AbstractBaseUser
from callhub.settings import AUTH_USER_MODEL as USER_MODEL
import pytz, hashlib, random, re
from timezone_field import TimeZoneField
from decimal import Decimal
from autoslug import AutoSlugField
from django_countries.fields import CountryField
from datetime import datetime, timedelta
from django.db.models.query import QuerySet
from django.utils.translation import ugettext, ugettext_lazy as _
from pytz import country_timezones
from django.dispatch import receiver
from django.db.models.signals import post_save
import json
from django.conf import settings

class CallHubUser(AbstractUser):
    CHOICES = (
        ('Expert', 'Expert'),
        ('Client', 'Client'),
    )
    image = models.ImageField(
        upload_to='profile_pic/', null=True, blank=True, default='profile_pic/default.jpg')
    avatar = models.CharField(null=True, blank=True, default="", max_length="255", verbose_name="Social Media Avatar",)
    user_type = models.CharField(
        choices=CHOICES, default='Client', max_length="255")
    headline = models.CharField(
        default='', blank=True, null=True, verbose_name="Headline", max_length="255")
    industry = models.CharField(
        default='', blank=True, null=True, verbose_name="Industry", max_length="255")

    about=models.TextField(
        default='', blank=True, null=True, verbose_name="About", max_length="3000", 
        help_text='About your self'
        )
    call_rate = models.FloatField(default=0.00, blank=True, null=True, verbose_name="Call Rate", help_text='Your Rate Per Minute')
    slug = AutoSlugField(populate_from=lambda instance: instance.get_unique_slug(),
                         unique_with='industry',
                         slugify = lambda value: value.replace(' ','-').replace('/', '-'),
                         always_update=False 
                         )

    current_status = models.CharField(default='', blank=True, null=True, verbose_name="Current Status",
                                      max_length="255")

    mobile_number = models.CharField(default='', blank=True, null=True, verbose_name="User Mobile Number",
                                     max_length="255")
    account_phone_number = models.CharField(default='', blank=True, null=True, verbose_name="Account Phone Number",
                                            max_length="255", help_text='This number is issuied by us')

    account_number_sid = models.CharField(default='', blank=True, null=True, verbose_name="Account Phone Number SID",
                                          max_length="255", help_text='This number is issuied by us')

    country = CountryField()
    timezone = TimeZoneField(null=True, blank=True)
    address = models.CharField(default='', blank=True, null=True, max_length=255)

    def save(self, *args, **kwargs):
        try:

            code = self.country.code
            timezone = country_timezones(code)
            if timezone:
                self.timezone = timezone[0]
            
        except Exception, e:
            print e
            pass

        super(CallHubUser, self).save(*args, **kwargs)    





    def get_fullname_or_username(self):
        if self.first_name:
            return '{0} {1}'.format(self.first_name, self.last_name)
        else:
            return self.username

    def get_fullname(self):
        
        return '{0} {1}'.format(self.first_name, self.last_name)
               

    def get_user_type(self):
        return self.user_type  

    def get_unique_slug(self):
        username = self.username
        if isinstance(username, unicode):
            username = username.encode('utf-8')
        slug = hashlib.sha1(username).hexdigest()

        return slug[:20]

           


def today():
    """
    Returns a tuple of two datetime instances: the beginning of today, and the
    end of today.
    """
    now = datetime.now()
    start = datetime.min.replace(year=now.year, month=now.month,
                                 day=now.day)
    end = (start + timedelta(days=1)) - timedelta.resolution
    return (start, end)
            


class SlotQuerySet(QuerySet):

    """
    A very simple ``QuerySet`` subclass which adds only one extra method,
    ``today``, which returns only those objects whose ``creation_date`` falls
    within the bounds of today.
    """

    def today(self):
        """
        Filters down to only those objects whose ``creation_date`` falls within
        the bounds of today.
        """
        return self.filter(created__range=today())


class SlotManager(models.Manager):

    """
    A very simple ``Manager`` subclass which returns an ``SlotQuerySet``
    instead of the typical ``QuerySet``.  It also includes a proxy for the extra
    ``today`` method that is provided by the ``SlotQuerySet`` subclass.
    """

    def get_queryset(self):
        """
        Gets an ``SlotQuerySet`` instead of a typical ``QuerySet``.
        """
        return SlotQuerySet(self.model)

    def today(self):
        """
        A proxy method for the extra ``today`` method that is provided by the
        ``SlotQuerySet`` subclass.
        """
        return self.get_queryset().today()



class Slot(models.Model):
    """
    this is going to be our event model, going to see if we can get an abstract class to act
    as our event class
    """
    start = models.DateTimeField(_("start"))
    end = models.DateTimeField(_("end"), help_text=_("The end time must be later than the start time."))
    title = models.CharField(_("title"), max_length=255, default='')
    description = models.TextField(_("description"), null=True, blank=True)

    expert = models.ForeignKey(
        USER_MODEL, related_name='expert', verbose_name='Expert')
    customer = models.ForeignKey(
        USER_MODEL, related_name='customer', verbose_name='Customer', blank=True, null=True)
    
    created = models.DateTimeField(auto_now_add=True, verbose_name="Created")
    modified = models.DateTimeField(auto_now=True, verbose_name="Updated On" )
    
    objects = SlotManager()
    #category = models.ForeignKey(Category, blank=True, null=True)
    # category = models.ForeignKey(Category, blank=True, null=True)
    scheduled = models.BooleanField(default=False)
    allDay =  models.BooleanField(default = False)


    # def _get_slot_class(self):
    #     (start,end) = today()
    #     start = timezone.make_aware(start, timezone.get_default_timezone())
    #     end = timezone.make_aware(end, timezone.get_default_timezone())
    #     classname = ['event']

    #     if self.end > start and self.end < end:
    #         print self.id
    #         print self.end, start
    #         classname.append('bg-color-orange')

    #         return classname
    #     elif (self.end > end):
    #         return json.dumps(["event", "bg-color-blue"])

    #     else:
    #         return json.dumps(['event', 'bg-color-red'])

    # className = property(_get_slot_class)        



    

    
    class Meta:
        verbose_name_plural = 'Slots'
        verbose_name = 'Slot'


    def __unicode__(self):
        return "Slot booked by {0} title {1}".format(self.customer, self.title)

    def today(self):
        """
        Determines whether this event takes place today or notself.
        """
        (start, end) = today()
        return self.start >= start and self.end <= end        


class OverallRating(models.Model):
    """
    this is the overall rating model, gives the total rating of all rating to a user
    e.g average of the ratings and the number of people who have rated
    """
    
    expert = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='expert_overall_rating')

    rating = models.DecimalField(decimal_places=1, max_digits=6, null=True, default=0 )
    number_of_ratings = models.IntegerField(default=0)

    
    class Meta:
        verbose_name='Overall Rating'
        verbose_name_plural = 'Overall Ratings'
        
    def __unicode__(self):
        return '{0} ({1})'.format(self.expert, self.rating)
    
    def update(self,incoming_score):
        """
        calculate the new overall rating based on old rating, number of rating and
        the new incomming rating
        """


        if self.rating == 0 and self.number_of_ratings == 1:
            self.rating = incoming_score

        else:
            current_rating = self.rating
            new_rating = ((current_rating*self.number_of_ratings)+incoming_score)/(self.number_of_ratings + 1)

            self.rating = Decimal(str(new_rating or "0"))

        self.save()

class Rating(models.Model):
    """
     incoming rating from the client
    """
    overall_rating = models.ForeignKey(OverallRating, null = True, related_name = "overall_rating")
    expert_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='expert_rating')
    customer_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='client_rating')
    rating = models.IntegerField()

    # comment = models.ForeignKey(Comment, null=True, blank=True)

    timestamp = models.DateTimeField(default=datetime.now)

    
    class Meta:
        verbose_name='Rating'
        verbose_name_plural = 'Ratings'

    def __unicode__(self):
        return '{0} from {1} to {2}'.format(self.rating, self.customer_user, self.expert_user)


